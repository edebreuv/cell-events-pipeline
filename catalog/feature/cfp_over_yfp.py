from typing import Tuple

from cell_tracking_BC.type.compartment.cell import cell_t


def AverageCFPOverYFP(
    cell: cell_t, /, *, background_suffix: str = ""
) -> Tuple[float, float]:
    """"""
    features = cell.features
    with_background = features["CFP"] / (features["YFP"] + 0.1)
    wo_background = features[f"CFP{background_suffix}"] / (
        features[f"YFP{background_suffix}"] + 0.1
    )

    return with_background, wo_background
