import numpy as nmpy
from cell_tracking_BC.type.compartment.cell import cell_t

array_t = nmpy.ndarray


def CellEdginess(cell: cell_t, frame: array_t, /) -> array_t:
    """
    Gets the cell edges by searching for the max cardinal jump
    """
    row_shifts = (-1, 0, 1, -1, 1, -1, 0, 1)
    col_shifts = (-1, -1, -1, 0, 0, 1, 1, 1)
    profile_length = 5
    out_dist_threshold = (profile_length // 2) + 1

    n_shifts = row_shifts.__len__()
    cardinal_jumps = nmpy.zeros(n_shifts, dtype=nmpy.float64)

    roi_map = cell.Map(frame.shape, as_boolean=True)
    row_col = []

    origin_row, origin_col = nmpy.around(cell.centroid).astype(int)
    rolling_profile = nmpy.empty(profile_length, dtype=nmpy.float64)
    for line_idx in range(n_shifts):
        row = origin_row
        col = origin_col
        out_dist = 0
        prof_idx = 0
        rolling_profile.fill(0)

        while out_dist < out_dist_threshold:
            prev_intensity = float(frame[row, col])

            row += row_shifts[line_idx]
            if (row < 0) or (row >= frame.shape[0]):
                break
            col += col_shifts[line_idx]
            if (col < 0) or (col >= frame.shape[1]):
                break

            prof_idx += 1  # move to the next profile index
            # save the difference between previous and current intensities
            rolling_profile[prof_idx % profile_length] = prev_intensity - float(
                frame[row, col]
            )
            row_col.append((row, col))

            if not roi_map[row, col]:
                out_dist += 1

        cardinal_jumps[line_idx] = max(rolling_profile)

    return cardinal_jumps
