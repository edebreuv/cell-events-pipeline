from typing import Dict

import numpy as nmpy
from skimage import exposure as xpsr

array_t = nmpy.ndarray


def RescaledPOLChannel(channels: Dict[str, array_t], /) -> array_t:
    """"""
    frame = channels["POL"]

    in_range = nmpy.around(nmpy.percentile(frame, (5, 95))).astype(nmpy.uint64)
    rescaled = xpsr.rescale_intensity(frame, in_range=tuple(in_range))

    return 255.0 * rescaled
