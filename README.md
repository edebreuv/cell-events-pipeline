# Dynamic analysis framework to detect cell division and cell death events in live-cell imaging experiments, using signal processing and machine learning

This dynamic analysis framework is a computational pipeline which runs an automatic analysis on a live-cell time-lapse microscopy, to detect cell divisions and cell deaths. It requires two types of channels that are commonly used in time-lapse microscopy experiments: a fluorescent channel to segment (i.e. find the regions of) the objects of interest (not limited to nuclei, cytoplasms or whole cells), and a polarized channel to detect cell death events. Unlike other dynamic analysis frameworks, this workflow does not require a specific channel for tracking cells nor a specific marker for cell death detection.

___

**Note 1:** The description below is outdated, but it still gives a general idea of the project.

**Note 2:** This repository is linked to a [Python Package Index (PyPI)](https://pypi.org/) project containing the main library [``cell_tracking_BC``](https://pypi.org/project/cell-tracking-bc/) of the dynamics analysis framework.

___



## Overview

The following description focuses on cells. The main steps of the dynamic analysis pipeline are:

- Segment the cells on the successive frames of the appropriate fluorescent channel using a convolutional neural network (CNN).
- Starting from the segmentation of the first frame, track the position of each cell from a segmented frame to the next, taking into account cell divisions, in order to build its tree-shaped, XYT-trajectory. Note that this trajectory tree might be just a trunk.
- In each trajectory, detect **cell divisions** as branch creations.
- Along each trajectory, extract the signal within the cell on the corresponding frames of the polarized channel, and *summarize* it into a scalar value to build a time series.
- Apply pattern matching to each time series, and detect **cell death** as the time-point where the matching score is above a given threshold or reaches a maximum.



## Details

The CNN currently used for cell segmentation comes from the U-Net publication (see Reference below). Switching to another architecture, say [Cellpose](https://cellpose.readthedocs.io/en/latest/), to adapt the pipeline to various cell types is only a matter of changing a parameter of the pipeline.

The cell death event detection currently use the following elements:

- The polarized signal "summarizing" value is the [Shannon entropy](https://en.wikipedia.org/wiki/Entropy_%28information_theory%29). (The signal variance has also been tested as an alternative, but revealed to be a bit less accurate for our purpose.)

- The cell death pattern is an inverted (i.e. decreasing) [sigmoid function](https://en.wikipedia.org/wiki/Sigmoid_function).



## Implementation

The programming language of the pipeline is Python 3. Beside the main library [``cell_tracking_BC``](https://pypi.org/project/cell-tracking-bc/), it relies on several open-source projects. The main entry point is the ``run_pipeline.py`` script.



## References

Ronneberger O, Fischer P, Brox T. U-Net: Convolutional Networks for Biomedical Image Segmentation, MICCAI, 2015.
