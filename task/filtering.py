from typing import Optional, Sequence

import cell_tracking_BC.task.matching.pattern as ptmt
import scipy.signal as sgnl
from numpy import ndarray as array_t

_MEDIAN_FILTER_SIZE = 15


def PatternMatchingResponse(
    signal: Sequence[float], pattern: array_t, task: str, /, *, track_label: int = None
) -> Optional[array_t]:
    """"""
    filter_size = min(_MEDIAN_FILTER_SIZE, signal.__len__())
    if filter_size % 2 == 0:
        filter_size -= 1
    if filter_size < 3:
        raise ValueError(f"{filter_size}: Invalid filter size. Expected>=3.")
    smoothed = sgnl.medfilt(sgnl.medfilt(signal, filter_size), filter_size)
    output = ptmt.match_template(
        smoothed,
        pattern,
        normalized="template",
        pad_input=True,
        mode="edge",
        calling_context=f"{task} Response of Track {track_label}",
    )

    return output
