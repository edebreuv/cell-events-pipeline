import numpy as nmpy
import tifffile as tiff
from im_tools_36.input import ImageVolumeOrSequence
from skimage import exposure
from skimage.transform import resize as Resized

array_t = nmpy.ndarray


def InputsAndOutput(
    sequence_path: str, layer_path: str, layer_name: str, /
) -> tuple[array_t, array_t, array_t]:
    """"""
    print(f"Loading sequence/{layer_name}...")
    sequence = ImageVolumeOrSequence(sequence_path)
    layer = ImageVolumeOrSequence(layer_path)

    if (s_length := sequence.shape[0]) != (l_length := layer.shape[0]):
        print(f"Fixing lengths of sequence[{s_length}]/{layer_name}[{l_length}]...")
        if s_length > l_length:
            sequence = sequence[:l_length, ...]
        else:
            layer = layer[:s_length, ...]

    if (s_shape := sequence.shape[1:]) != (l_shape := layer.shape[1:]):
        print(
            f"WARNING:\n"
            f"    Sequence and {layer_name} have different shapes: "
            f"{s_shape} and {l_shape}, resp.\n"
            f"    Resizing sequence..."
        )
        d_type = sequence.dtype
        sequence = Resized(sequence, (layer.shape[0], *l_shape), preserve_range=True)
        sequence = nmpy.around(sequence).astype(d_type)

    shape = list(sequence.shape)
    shape[1] = 3
    output = nmpy.empty(shape, dtype=nmpy.uint8)

    print(f"Data shapes: {sequence.shape}")
    assert sequence.shape == layer.shape
    assert sequence.shape == output.shape

    return sequence, layer, output


def TransferChannelToOutput(
    sequence: array_t,
    channel_idx: int,
    # should_flip_vertically: bool,
    output: array_t,
    /,
) -> None:
    """"""
    print("Normalizing and copying channel...")
    channel = sequence[:, channel_idx, ...]
    channel = exposure.equalize_adapthist(channel, clip_limit=0.03)  # CLAHE
    channel = nmpy.around(255.0 * channel).astype(nmpy.uint8)
    # if should_flip_vertically:
    #     channel = nmpy.flip(channel, axis=1)

    for c_idx in range(3):
        output[:, c_idx, ...] = channel


def SaveOutput(output: array_t, output_path: str, /) -> None:
    """"""
    print("Saving output...")
    tiff.imwrite(
        output_path,
        output,
        photometric="rgb",
        compression="deflate",
        planarconfig="separate",
        metadata={"axes": "XYZCT"},
    )
