"""
python task/annotations-on-channel.py [sequence] 2 .../sequence_annotations.tiff 255,255,0 /tmp/out.tiff
"""

import sys as sstm
from pathlib import Path as path_t

import numpy as nmpy
from x_on_channel import InputsAndOutput, SaveOutput, TransferChannelToOutput

if sstm.argv.__len__() != 6:
    print("Incorrect script call; Please check script code for help.")
    sstm.exit(1)

try:
    sequence_path = sstm.argv[1]  # Path as str.
    channel_idx = int(sstm.argv[2])  # Channel index starting from zero.
    annotations_path = sstm.argv[3]  # Path as str.
    color = sstm.argv[4]  # 255-based, comma-separated, RBG color; e.g., 255,0,0
    output_path = sstm.argv[5]  # Path as str. Never overwritten.
except:
    print("Incorrect script call; Please check script code for help.")
    sstm.exit(1)

if path_t(output_path).exists():
    print(f"Output path already exists: {output_path}.")
    sstm.exit(1)

print(
    f"Superimposing...\n"
    f"    annotations  {annotations_path}\n"
    f"    onto channel {channel_idx}\n"
    f"    of sequence  {sequence_path}\n"
    f"    with color   ({color})."
)

sequence, annotations, output = InputsAndOutput(
    sequence_path, annotations_path, "annotations"
)
TransferChannelToOutput(sequence, channel_idx, output)  # , True, output)

print("Superimposing annotations...")
# annotations_gray = (
#     0.2125 * annotations[:, 0, ...]
#     + 0.7154 * annotations[:, 1, ...]
#     + 0.0721 * annotations[:, 2, ...]
# )
annotations_combined = (
    annotations[:, 0, ...] + annotations[:, 1, ...] + annotations[:, 2, ...]
)
annotations_combined = (annotations_combined - nmpy.amin(annotations_combined)) / (
    nmpy.amax(annotations_combined) - nmpy.amin(annotations_combined)
)
where_annotations = annotations_combined > 0
values = map(int, color.split(","))
for c_idx, value in enumerate(values):
    value_weight = annotations_combined[where_annotations]
    output[:, c_idx, ...][where_annotations] = nmpy.around(
        value * value_weight
        + output[:, c_idx, ...][where_annotations] * (1.0 - value_weight)
    )

SaveOutput(output, output_path)
