from __future__ import annotations

import dataclasses as dtcl
import os as opss
import sys as sstm
import typing as h
from pathlib import Path as path_t

import numpy as nmpy
import openpyxl as xlsx
from openpyxl.worksheet.worksheet import Worksheet as worksheet_t

thread_label_t = int
track_label_t = int  # Forking track.
sibling_h = tuple[thread_label_t, ...]
track_siblings_h = dict[track_label_t, sibling_h]
#
division_time_t = int
division_record_h = tuple[sibling_h, division_time_t]
track_divisions_per_thread_h = dict[thread_label_t, tuple[division_time_t, ...]]
track_division_records_h = dict[track_label_t, tuple[division_record_h, ...]]
tracks_division_records_h = track_division_records_h

BASE_FOLDER = "BASE_FOLDER"

VALID_MARKER = "Valid"
THREAD_MARKER = "Thread track"
NOT_IN_GT = "Not in GT"
OVERLAP_MARKER = "Overlap"
UNWANTED_MARKER = "Unwanted"
# OVERLAP_CODE = -1
# UNWANTED_CODE = -2


@dtcl.dataclass(slots=True, repr=False, eq=False)
class worksheets_t:
    siblings: worksheet_t
    validity: worksheet_t
    divisions: worksheet_t

    @classmethod
    def NewForPaths(cls, tracks: path_t, divisions: path_t, /) -> worksheets_t:
        """"""
        siblings = xlsx.load_workbook(tracks)["siblings"]
        validity = xlsx.load_workbook(tracks)["status"]
        divisions = xlsx.load_workbook(divisions)["division times"]

        for sheet in (siblings, validity, divisions):
            assert getattr(sheet, "min_row") == 1, getattr(sheet, "min_row")
            assert getattr(sheet, "min_column") == 1, getattr(sheet, "min_column")

        assert (siblings.max_row == validity.max_row) and (
            siblings.max_row == divisions.max_row
        ), (siblings.max_row, validity.max_row, divisions.max_row)

        return cls(siblings=siblings, validity=validity, divisions=divisions)


@dtcl.dataclass(slots=True, repr=False, eq=False)
class division_record_summary_t:
    name: str
    n_thread_tracks: int = 0
    n_forking_tracks: int = 0
    n_divisions: int = 0
    # n_overlaps: int = 0
    # n_unwanted: int = 0

    def __str__(self) -> str:
        """"""
        output = []

        for attribute in dtcl.fields(division_record_summary_t):
            attribute = attribute.name
            value = getattr(self, attribute)
            output.append(f"{attribute}: {value}")

        return "\n".join(output)

    def DivisionAccuracy(self, reference: division_record_summary_t, /) -> float:
        """"""
        return (
            100.0
            - 100.0
            * abs(self.n_divisions - reference.n_divisions)
            / reference.n_divisions
        )

    def Update(self, other: division_record_summary_t, /) -> None:
        """"""
        for attribute in dtcl.fields(self.__class__):
            attribute = attribute.name
            value = getattr(self, attribute)
            if isinstance(value, (int, float)):
                setattr(self, attribute, value + getattr(other, attribute))

    def PrintComparatively(self, reference: division_record_summary_t, /) -> None:
        """"""
        # assert (self.n_overlaps == 0) and (self.n_unwanted == 0), str(self)

        accuracy = self.DivisionAccuracy(reference)
        print(
            f"N Thread tracks GT/DN={reference.n_thread_tracks}/{self.n_thread_tracks}",
            f"N Forking tracks GT/DN={reference.n_forking_tracks}/{self.n_forking_tracks}",
            f"N Divisions GT/DN/Acc={reference.n_divisions}/{self.n_divisions}/{accuracy:.1f}",
            sep="\n",
        )
        # f"FP will include {reference.n_overlaps} overlap(s) and {reference.n_unwanted} unwanted(s)",


@dtcl.dataclass(slots=True, repr=False, eq=False)
class division_record_t(division_record_summary_t):
    valid_labels: tuple[thread_label_t, ...] | None = None
    siblings: dict[track_label_t, tuple[thread_label_t]] | None = None
    thread_labels: tuple[thread_label_t, ...] | None = None
    tddpt_s: tuple[track_divisions_per_thread_h] | None = None
    tracks_division_records: tracks_division_records_h | None = None

    def __str__(self) -> str:
        """"""
        output = []

        if self.valid_labels is not None:
            output.append(f"Valid labels: {str(self.valid_labels)[1:-1]}")
        if self.thread_labels is not None:
            output.append(f"Thread track labels: {str(self.thread_labels)[1:-1]}")
        if self.siblings is not None:
            siblings = (
                f"{_key}: {str(_vle)[1:-1]}" for _key, _vle in self.siblings.items()
            )
            siblings = "; ".join(siblings)
            output.append(f"Siblings: {siblings}")
        if self.tddpt_s is not None:
            tddpt_s = (
                ", ".join(f"{_key}: {str(_vle)[1:-1]}" for _key, _vle in _elm.items())
                for _elm in self.tddpt_s
            )
            tddpt_s = "; ".join(tddpt_s)
            output.append(f"TDDpT: {tddpt_s}")
        if self.tracks_division_records is not None:
            tracks_division_records = (
                f"{_key}: {str(_vle)[1:-1]}"
                for _key, _vle in self.tracks_division_records.items()
            )
            tracks_division_records = ", ".join(tracks_division_records)
            output.append(f"Tracks division records: {tracks_division_records}")

        return division_record_summary_t.__str__(self) + "\n" + "\n".join(output)

    def Update(self, _: division_record_summary_t, /) -> None:
        """"""
        raise RuntimeError("Only record summaries can be updated.")

    def SetValidLabels(self, validity: worksheet_t, /) -> None:
        """"""
        validity_s = tuple(_elm == VALID_MARKER for _elm in SheetColumn(validity, 1))
        self.valid_labels = tuple(nmpy.nonzero(validity_s)[0] + 1)

    @property
    def n_valid_labels(self) -> int:
        """
        Should be equal to: n_thread_tracks + sum_"n threads in each forking track".
        """
        return self.valid_labels.__len__()

    def SetSiblings(self, worksheets: worksheets_t, /) -> None:
        """"""
        siblings = {}
        thread_labels = []
        n_thread_tracks = 0

        not_visited = list(self.valid_labels)
        while not_visited.__len__() > 0:
            current = not_visited[0]
            del not_visited[0]

            sibling = SheetRow(worksheets.siblings, current, should_be_int=False)
            # if (sibling is None) or (sibling[0] == THREAD_MARKER):
            if (sibling is None) or (sibling[0] in (THREAD_MARKER, NOT_IN_GT)):
                thread_labels.append(current)
                n_thread_tracks += 1
            else:
                if isinstance(sibling[0], str):
                    sibling = tuple(map(int, sibling[0].split(",")))
                siblings[current] = sibling
                for label in sibling:
                    if label in self.valid_labels:
                        not_visited.remove(label)

        self.siblings = siblings
        self.thread_labels = tuple(thread_labels)
        self.n_thread_tracks = n_thread_tracks

        assert sorted(set(thread_labels).union(SiblingThreads(siblings))) == list(
            self.valid_labels
        ), str(self)
        assert n_thread_tracks == thread_labels.__len__(), str(self)
        assert (
            n_thread_tracks == self.n_valid_labels - SiblingThreads(siblings).__len__()
        ), str(self)

    def SetNForkingTracks(self) -> None:
        """"""
        self.n_forking_tracks = self.siblings.__len__()

    def SetTracksDivisionsDetailedPerThread(self, divisions: worksheet_t, /) -> None:
        """"""
        tddpt_s = TracksDivisionsDetailedPerThread(
            divisions, self.valid_labels, self.siblings
        )
        if isinstance(tddpt_s, str):
            print(tddpt_s)
            sstm.exit(1)

        self.tddpt_s = tddpt_s

        assert self.tddpt_s.__len__() == tuple(self.siblings.keys()).__len__(), str(
            self
        )
        assert self.thread_labels == ThreadTracksLabels(
            self.valid_labels, self.tddpt_s
        ), f"{self}\n{ThreadTracksLabels(self.valid_labels, self.tddpt_s)}"
        assert self.n_thread_tracks == self.n_valid_labels - sum(
            map(lambda _elm: _elm.keys().__len__(), self.tddpt_s)
        ), str(self)
        assert self.n_forking_tracks == self.tddpt_s.__len__(), str(self)

    def SetTracksDivisionsRecords(self) -> None:
        """"""
        tracks_division_records = {}
        for tddpt in self.tddpt_s:
            (track_division_records, n_overlaps, n_unwanted) = TrackDivisionRecords(
                tddpt
            )
            tracks_division_records.update(track_division_records)
            # self.n_overlaps += n_overlaps
            # self.n_unwanted += n_unwanted

        self.tracks_division_records = tracks_division_records

    def PrintComparatively(self, reference: division_record_t, /) -> None:
        """"""
        # assert (self.n_overlaps == 0) and (
        #     self.n_unwanted == 0
        # ), f"Self:\n{self}\nReference:\n{reference}"

        accuracy = self.DivisionAccuracy(reference)
        print(
            f"N Thread tracks GT/DN={reference.n_thread_tracks}/{self.n_thread_tracks}:\n"
            f"    GT: {str(reference.thread_labels)[1:-1]}\n"
            f"    DN: {str(self.thread_labels)[1:-1]}\n"
            f"N Forking tracks GT/DN={reference.n_forking_tracks}/{self.n_forking_tracks}:\n"
            f"    GT: {str(tuple(reference.siblings.keys()))[1:-1]}\n"
            f"    DN: {str(tuple(self.siblings.keys()))[1:-1]}\n"
            f"N Divisions GT/DN/Accuracy={reference.n_divisions}/{self.n_divisions}/{accuracy:.1f}"
        )
        # f"FP will include {reference.n_overlaps} overlap(s) and {reference.n_unwanted} unwanted(s)"


@dtcl.dataclass(slots=True, repr=False, eq=False)
class division_measures_t:
    n_true_p: int = 0
    n_false_p: int = 0
    n_true_n: int = 0
    n_false_n: int = 0
    n_overlaps: int = 0
    n_unwanted: int = 0
    true_p: tuple[thread_label_t | track_label_t, ...] | None = None
    false_p: tuple[thread_label_t | track_label_t, ...] | None = None
    true_n: tuple[thread_label_t | track_label_t, ...] | None = None
    false_n: tuple[thread_label_t | track_label_t, ...] | None = None

    def SetForDetectionAndReference(
        self,
        detection: division_record_t,
        reference: division_record_t,
        divisions_gt: worksheet_t,
        /,
    ) -> None:
        """"""
        self.true_p, self.false_p, self.false_n, *others = Accuracy(
            reference.tracks_division_records,
            detection.tracks_division_records,
            divisions_gt,
        )
        self.n_true_p, self.n_false_p, self.n_false_n = (
            self.true_p.__len__(),
            self.false_p.__len__(),
            self.false_n.__len__(),
        )
        if others.__len__() > 0:
            self.n_overlaps = others[0].__len__()
            self.n_unwanted = others[1].__len__()

        self.true_n = tuple(
            set(reference.thread_labels).intersection(detection.thread_labels)
        )
        self.n_true_n = self.true_n.__len__()

    def Update(self, other: division_measures_t, /) -> None:
        """"""
        for attribute in dtcl.fields(self.__class__):
            attribute = attribute.name
            value = getattr(self, attribute)
            if isinstance(value, (int, float)):
                setattr(self, attribute, value + getattr(other, attribute))

    def PrintForNormalizations(
        self,
        detection: division_record_summary_t,
        reference: division_record_summary_t,
        /,
    ) -> None:
        """"""
        if detection.n_divisions > 0:
            tpr_or_nan = 100.0 * self.n_true_p / detection.n_divisions
        else:
            tpr_or_nan = nmpy.NaN
        if reference.n_divisions > 0:
            fnr_or_nan = 100.0 * self.n_false_n / reference.n_divisions
        else:
            fnr_or_nan = nmpy.NaN
        if detection.n_divisions > 0:
            fpr_or_nan = 100.0 * self.n_false_p / detection.n_divisions
        else:
            fpr_or_nan = nmpy.NaN
        if reference.n_thread_tracks > 0:
            tnr_or_nan = 100.0 * self.n_true_n / reference.n_thread_tracks
        else:
            tnr_or_nan = nmpy.NaN
        print(
            f"TP FN = {self.n_true_p:3} {self.n_false_n:3}",
            f"FP TN = {self.n_false_p:3} {self.n_true_n:3}",
            f"TPR FNR = {tpr_or_nan:4.1f} {fnr_or_nan:4.1f}",
            f"FPR TNR = {fpr_or_nan:4.1f} {tnr_or_nan:4.1f}",
            f"FP include(s) around {self.n_overlaps} overlap(s) and "
            f"{self.n_unwanted} unwanted(s)",
            sep="\n",
        )
        if self.true_p is not None:
            print(
                f"TP: {self.true_p}",
                f"FP: {self.false_p}",
                f"TN: {self.true_n}",
                f"FN: {self.false_n}",
                sep="\n",
            )


def SheetRow(
    sheet: worksheet_t, label: int, /, *, should_be_int: bool = True
) -> h.Any | tuple[int, ...] | None:
    """"""
    output = []

    row = tuple(
        sheet.iter_cols(
            min_col=1,
            max_col=sheet.max_column,
            min_row=label,
            max_row=label,
            values_only=True,
        )
    )
    row = tuple(_elm[0] for _elm in row if _elm[0] is not None)
    if (row.__len__() == 0) or (row[0] is None):
        return None

    for element in row:
        if element is None:
            break
        if should_be_int:
            try:
                value = int(element)
            except ValueError:
                # if element == OVERLAP_MARKER:
                #     value = OVERLAP_CODE
                # elif element == UNWANTED_MARKER:
                #     value = UNWANTED_CODE
                # else:
                break
        else:
            value = element
        output.append(value)

    if output.__len__() > 0:
        return tuple(output)

    return None


def SheetColumn(sheet: worksheet_t, col: int, /) -> tuple[h.Any, ...]:
    """"""
    return tuple(
        _elm[0]
        for _elm in sheet.iter_rows(
            min_col=col,
            max_col=col,
            min_row=1,
            max_row=sheet.max_row - 1,
            values_only=True,
        )
    )


def TracksDivisionsDetailedPerThread(
    sheet: worksheet_t,
    valid_labels: tuple[thread_label_t],
    siblings: track_siblings_h,
    /,
) -> tuple[track_divisions_per_thread_h] | str:
    """"""
    output = []

    thread_labels = set(valid_labels)
    for track_label, sibling in siblings.items():
        forking_labels = (track_label,) + sibling
        thread_labels.difference_update(forking_labels)
        tddpt = {}
        for thread_label in forking_labels:
            divisions = SheetRow(sheet, thread_label)
            if divisions is not None:
                # divisions = tuple(_elm for _elm in divisions if _elm >= 0)
                # if divisions.__len__() > 0:
                # Otherwise, it must be the ground-truth having no division where one
                # was detected by the pipeline.
                tddpt[thread_label] = divisions

        if tddpt.__len__() > 0:
            # Otherwise, it must be the ground-truth having no divisions where some
            # were detected by the pipeline.
            output.append(tddpt)

    new_forking_labels = []
    for thread_label in thread_labels:
        divisions = SheetRow(sheet, thread_label)
        if divisions is not None:
            divisions = tuple(_elm for _elm in divisions if _elm >= 0)
            if divisions.__len__() > 0:
                new_forking_labels.append(thread_label)
    if new_forking_labels.__len__() > 0:
        return f"/!\\ NEW FORKING LABELS: {new_forking_labels}; Please update TRACKS."

    return tuple(output)


def TrackDivisionRecords(
    tddpt: track_divisions_per_thread_h, /
) -> tuple[track_division_records_h, int, int]:
    """"""
    output = []
    n_overlaps = 0
    n_unwanteds = 0

    labels, times_all = zip(*tddpt.items())
    labels = nmpy.array(tuple(labels), dtype=nmpy.int64)
    times_all = tuple(times_all)

    max_n_times = max(map(len, times_all))
    grouped = nmpy.full((labels.size, max_n_times), nmpy.NAN, dtype=nmpy.float64)
    for row, times in enumerate(times_all):
        grouped[row, : times.__len__()] = times

    for col in range(grouped.shape[1]):
        column = grouped[:, col]
        for time in nmpy.unique(column):
            if nmpy.isnan(time):
                continue
            where = nmpy.nonzero(column == time)[0]
            time = int(time)
            # if time == OVERLAP_CODE:
            #     n_overlaps += 1
            # elif time == UNWANTED_CODE:
            #     n_unwanteds += 1
            # else:
            output.append((tuple(labels[where]), time))

    return {nmpy.amin(labels).item(): tuple(output)}, n_overlaps, n_unwanteds


def Accuracy(
    ground_truth: tracks_division_records_h,
    detection: tracks_division_records_h,
    divisions_gt: worksheet_t,
    /,
) -> tuple[
    tuple[tuple[int, int], ...],
    tuple[tuple[int, int], ...],
    tuple[tuple[int, int], ...],
    tuple[int, ...],
    tuple[int, ...],
]:
    """"""

    def _Accuracy(
        current: tracks_division_records_h,
        reference: tracks_division_records_h,
        /,
        *,
        overlap_s_: list[int] | None = None,
        unwanted_s_: list[int] | None = None,
    ) -> tuple[tuple[tuple[int, int], ...], tuple[tuple[int, int], ...]]:
        """"""
        true_s, false_s = [], []

        for track_label, division_records in current.items():
            row = SheetRow(divisions_gt, track_label, should_be_int=False)

            reference_records = reference.get(track_label)
            if reference_records is None:
                false_s.extend((track_label, _rcd[1]) for _rcd in division_records)
                # TODO: This is only an approximation of the number of overlaps and
                #     unwanted's.
                if overlap_s_ is not None:
                    if OVERLAP_MARKER in row:
                        overlap_s_.append(track_label)
                    elif UNWANTED_MARKER in row:
                        unwanted_s_.append(track_label)
                continue

            for record in division_records:
                label_time = (track_label, record[1])
                if record in reference_records:
                    true_s.append(label_time)
                else:
                    false_s.append(label_time)
                    # TODO: This is only an approximation of the number of overlaps and
                    #     unwanted's.
                    if overlap_s_ is not None:
                        if OVERLAP_MARKER in row:
                            overlap_s_.append(track_label)
                        elif UNWANTED_MARKER in row:
                            unwanted_s_.append(track_label)

        return tuple(true_s), tuple(false_s)

    overlap_s = []
    unwanted_s = []
    true_p_s, false_p_s = _Accuracy(
        detection, ground_truth, overlap_s_=overlap_s, unwanted_s_=unwanted_s
    )
    _, false_n_s = _Accuracy(ground_truth, detection)

    return true_p_s, false_p_s, false_n_s, tuple(overlap_s), tuple(unwanted_s)


def SiblingThreads(sibling: track_siblings_h, /) -> tuple[thread_label_t, ...]:
    """"""
    output = list(sibling.keys())

    for value in sibling.values():
        output.extend(value)

    return tuple(set(output))


def ThreadTracksLabels(
    valid_labels: tuple[thread_label_t, ...],
    tddpt_s: tuple[track_divisions_per_thread_h],
    /,
) -> tuple[thread_label_t, ...]:
    """"""
    thread_labels = []
    for tddpt in tddpt_s:
        # labels = (
        #     _lbl
        #     for _lbl, _lst in tddpt.items()
        #     if _lst[0] not in (OVERLAP_CODE, UNWANTED_CODE)
        # )
        # thread_labels.extend(labels)
        thread_labels.extend(tddpt.keys())

    return tuple(sorted(set(valid_labels).difference(thread_labels)))


def Main(tg_path: str, td_path: str, g_path: str, d_path: str, /) -> None:
    """"""
    try:
        base_folder = opss.environ[BASE_FOLDER]
    except KeyError:
        base_folder = None
    if base_folder is not None:
        tg_path = tg_path.replace(f"${BASE_FOLDER}", base_folder)
        td_path = td_path.replace(f"${BASE_FOLDER}", base_folder)
        g_path = g_path.replace(f"${BASE_FOLDER}", base_folder)
        d_path = d_path.replace(f"${BASE_FOLDER}", base_folder)

    tg_path = path_t(tg_path)
    td_path = path_t(td_path)
    g_path = path_t(g_path)
    d_path = path_t(d_path)

    all_paths = (tg_path, td_path, g_path, d_path)
    assert all(_elm.is_file() for _elm in all_paths) or all(
        _elm.is_dir() for _elm in all_paths
    ), all_paths

    if tg_path.is_file():
        tg_paths = (tg_path,)
        td_paths = (td_path,)
        g_paths = (g_path,)
        d_paths = (d_path,)
    else:
        tg_paths = tg_path.glob("*.xlsx")
        td_paths = td_path.glob("*.xlsx")
        g_paths = g_path.glob("*.xlsx")
        d_paths = d_path.glob("*.xlsx")

    record_gt_total = division_record_summary_t(name="Ground-truth (total)")
    record_dn_total = division_record_summary_t(name="Detection (total)")
    measures_total = division_measures_t()

    for tg_path, td_path, g_path, d_path in zip(tg_paths, td_paths, g_paths, d_paths):
        print(
            f"----\n"
            f"FILES:\n"
            f"    Tracks GT=   {tg_path.name}\n"
            f"    Tracks DN=   {td_path.name}\n"
            f"    Ground-truth={g_path.name}\n"
            f"    Detection=   {d_path.name}"
        )

        worksheets_gt = worksheets_t.NewForPaths(tg_path, g_path)
        worksheets_dn = worksheets_t.NewForPaths(td_path, d_path)

        record_gt = division_record_t(name="Ground-truth")
        record_dn = division_record_t(name="Detection")
        measures = division_measures_t()

        record_gt.SetValidLabels(worksheets_gt.validity)
        record_dn.SetValidLabels(worksheets_dn.validity)

        record_gt.SetSiblings(worksheets_gt)
        record_dn.SetSiblings(worksheets_dn)

        record_gt.SetNForkingTracks()
        record_dn.SetNForkingTracks()

        for divisions, record in (
            (worksheets_gt.divisions, record_gt),
            (worksheets_dn.divisions, record_dn),
        ):
            record.SetTracksDivisionsDetailedPerThread(divisions)
            record.SetTracksDivisionsRecords()

        measures.SetForDetectionAndReference(
            record_dn, record_gt, worksheets_gt.divisions
        )

        record_gt.n_divisions = measures.n_true_p + measures.n_false_n
        record_dn.n_divisions = measures.n_true_p + measures.n_false_p

        record_gt_total.Update(record_gt)
        record_dn_total.Update(record_dn)
        measures_total.Update(measures)

        record_dn.PrintComparatively(record_gt)
        measures.PrintForNormalizations(record_dn, record_gt)

    print("\n----\nTOTAL:")
    record_dn_total.PrintComparatively(record_gt_total)
    print("----")
    measures_total.PrintForNormalizations(record_dn_total, record_gt_total)


if __name__ == "__main__":
    #
    Main(sstm.argv[1], sstm.argv[2], sstm.argv[3], sstm.argv[4])
