"""
python task/segmentation-on-channel.py [sequence] 2 .../segmentation.tiff 255,255,0 3 /tmp/out.tiff
"""

import sys as sstm
from pathlib import Path as path_t

import numpy as nmpy
from skimage import morphology as mrph
from x_on_channel import InputsAndOutput, SaveOutput, TransferChannelToOutput

if sstm.argv.__len__() != 7:
    print("Incorrect script call; Please check script code for help.")
    sstm.exit(1)

try:
    sequence_path = sstm.argv[1]  # Path as str.
    channel_idx = int(sstm.argv[2])  # Channel index starting from zero.
    segmentation_path = sstm.argv[3]  # Path as str.
    color = sstm.argv[4]  # 255-based, comma-separated, RBG color; e.g., 255,0,0
    thickness = sstm.argv[5]  # Cell contour thickness.
    output_path = sstm.argv[6]  # Path as str. Never overwritten.
except:
    print("Incorrect script call; Please check script code for help.")
    sstm.exit(1)

if path_t(output_path).exists():
    print(f"Output path already exists: {output_path}.")
    sstm.exit(1)

print(
    f"Superimposing...\n"
    f"    segmentation {segmentation_path}\n"
    f"    onto channel {channel_idx}\n"
    f"    of sequence  {sequence_path}\n"
    f"    with color   ({color})."
)

sequence, segmentation, output = InputsAndOutput(
    sequence_path, segmentation_path, "segmentation"
)
TransferChannelToOutput(sequence, channel_idx, output)  # , False, output)

print("Extracting and superimposing contours...")
segmentation = segmentation[:, 0, ...]
eroded = nmpy.empty(segmentation.shape, dtype=nmpy.bool_)
for t_idx in range(segmentation.shape[0]):
    eroded[t_idx, ...] = segmentation[t_idx, ...]
    if nmpy.any(eroded[t_idx, ...]):
        for e_idx in range(int(thickness)):
            eroded[t_idx, ...] = mrph.binary_erosion(eroded[t_idx, ...])

contour = nmpy.logical_xor(segmentation, eroded)
values = map(int, color.split(","))
for c_idx, value in enumerate(values):
    output[:, c_idx, ...][contour] = value
# output = nmpy.flip(output, axis=2)

SaveOutput(output, output_path)
