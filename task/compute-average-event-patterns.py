"""
This module takes parametric patterns (effectively their parameters) individually fitted
on a set of sample signals, and outputs an average parametric pattern (effectively its
parameters. It does so for division and death events, each in the contexts of several
smoothing procedures of the sample signals (2 in practice: uniform and median).
"""

from pathlib import Path as path_t

import matplotlib.pyplot as pypl
import numpy as nmpy
import openpyxl as pyxl
from lmfit import Model

DIVISION = "division-normalized.xlsx"
DEATH = "death-normalized.xlsx"
STYLES = {
    DIVISION: {
        "uniform": {"amplitude": "b-", "width": "b--"},
        "medfilt": {"amplitude": "g-.", "width": "g:"},
        "uniform-fit": {"amplitude": "c-", "width": "c--"},
        "medfilt-fit": {"amplitude": "y-.", "width": "y:"},
    },
    DEATH: {
        "uniform": {"amplitude": "r-", "width": "r--"},
        "medfilt": {"amplitude": "m-.", "width": "m:"},
        "uniform-fit": {"amplitude": "c-", "width": "c--"},
        "medfilt-fit": {"amplitude": "y-.", "width": "y:"},
    },
}
PATTERN_AXES_OFFSET = {"uniform": 0, "medfilt": 1}

BASE_FOLDER = path_t(__file__).parent.parent.parent / "_data" / "pattern"


def GaussianPattern(x, cen, wid):
    """"""
    offset = 0.0
    amp = 1.0
    return offset + (amp / (nmpy.sqrt(2 * nmpy.pi) * wid)) * nmpy.exp(
        -((x - cen) ** 2) / (2 * wid**2)
    )


def DivisionPattern(x, amp, cen, wid, offset):
    """"""
    return offset - (amp / (nmpy.sqrt(2 * nmpy.pi) * wid)) * nmpy.exp(
        -((x - cen) ** 2) / (2 * wid**2)
    )


def DeathPattern(x, amp, cen, wid, offset):
    """"""
    return offset - amp / (1.0 + nmpy.exp(cen / wid - x / wid))


def FittingParameters(abscissas, signal, model):
    """"""
    fit = Model(model).fit(signal, x=abscissas, cen=nmpy.mean(abscissas), wid=3)

    return (fit.params["cen"].value, fit.params["wid"].value)


figure, all_axes = pypl.subplots(nrows=2, ncols=4)

for event, Pattern, all_evt_axes in zip(
    (DIVISION, DEATH), (DivisionPattern, DeathPattern), all_axes
):
    workbook = pyxl.load_workbook(BASE_FOLDER / event)

    for smoothing in workbook.sheetnames:
        sheet = workbook[smoothing]
        values = tuple(sheet.values)

        coeff_names = values[0]
        coefficients = nmpy.array(values[1:])

        # Per-smoothing fitted patterns
        n_widths = 3
        for amplitude, width in coefficients:
            abscissas = nmpy.linspace(
                -n_widths * width,
                n_widths * width,
                num=int(round(2 * n_widths * width)) + 1,
            )
            ordinates = Pattern(abscissas, amplitude, 0.0, width, 0.0)
            all_evt_axes[1 + PATTERN_AXES_OFFSET[smoothing]].plot(abscissas, ordinates)

        avg_parameters = []
        n_bins = 3 * int(round(nmpy.sqrt(coefficients.shape[0])))
        for col, coeff_name in enumerate(coeff_names):
            # Per-smoothing, per-coefficient, histogram of estimated coefficients
            counts, edges = nmpy.histogram(
                coefficients[:, col], bins=n_bins, density=True
            )
            centers = 0.5 * (edges[1:] + edges[:-1])
            all_evt_axes[0].plot(
                centers,
                counts,
                STYLES[event][smoothing][coeff_name],
                label=f"{smoothing}.{coeff_name}",
            )

            # Per-smoothing average coefficient and distribution
            center, *rest = FittingParameters(centers, counts, GaussianPattern)
            avg_parameters.append(center)
            all_evt_axes[0].plot(
                centers,
                GaussianPattern(centers, center, *rest),
                STYLES[event][f"{smoothing}-fit"][coeff_name],
                label=f"{smoothing}.fit-{coeff_name}={center:.1f}",
            )

        # Per-smoothing average pattern
        amplitude, width = avg_parameters
        n_widths = 10
        abscissas = nmpy.linspace(
            -n_widths * width,
            n_widths * width,
            num=int(round(2 * n_widths * width)) + 1,
        )
        ordinates = Pattern(abscissas, amplitude, 0.0, width, 0.0)
        all_evt_axes[3].plot(
            abscissas, ordinates, label=f"{smoothing}=a:{amplitude:.1f}, w:{width:.1f}"
        )
        print(f"{Pattern.__name__}: {smoothing}=a:{amplitude}, w:{width}")

    all_evt_axes[0].set_title(event)
    all_evt_axes[0].legend()
    all_evt_axes[3].legend()

pypl.show()
