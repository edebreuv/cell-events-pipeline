import time

import matplotlib.pyplot as p
import numpy as n
from config.unet import UNetInstance

FRAME_SHAPE = (512, 512)
WEIGHT_PATH = "../_data/weights/hela_p53_c8_cell.h5"

frame = n.full((*FRAME_SHAPE, 1), 100.0, dtype=n.float32)
network = UNetInstance(*frame.shape, WEIGHT_PATH)

e_times_predict = []
e_times_call = []
for n_frames in (10, 50, 100):
    print(f"{n_frames}...")

    frames = n.stack(n_frames * (frame,))

    time_1 = time.monotonic()
    predictions_predict = network.predict(frames, verbose=0)
    time_2 = time.monotonic()
    predictions_call = network(frames, training=False).numpy()
    time_3 = time.monotonic()

    e_times_predict.append(time_2 - time_1)
    e_times_call.append(time_3 - time_2)

print(
    "Absolute error on last predictions",
    max(n.abs(predictions_predict - predictions_call).flat),
)

p.plot(e_times_predict, label="predict")
p.plot(e_times_call, label="call")
p.legend()
p.show()
