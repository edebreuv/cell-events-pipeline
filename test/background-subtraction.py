import matplotlib.pyplot as pypl
import numpy as nmpy
import skimage as skim
from cell_tracking_BC.catalog.processing.background import WithBackgroundSubtracted

shape = (512, 512)
block_size = (81, 81)


# --- Image with Randon Shapes and Some Noise
shapes, _ = skim.draw.random_shapes(
    shape,
    10,
    min_shapes=5,
    min_size=20,
    max_size=40,
    channel_axis=None,
    intensity_range=(50, 100),
    random_seed=0,
)
shapes[shapes == 255] = 0
shapes = shapes.astype(nmpy.float64)

nmpy.random.seed(0)

shapes += 5.0 * nmpy.random.random(shape)


# --- Show Histograms in plain and log versions
block = shapes[290 : (290 + block_size[0]), 160 : (160 + block_size[1])]

n_bins = max(block.size // 100, 10)
counts, edges = nmpy.histogram(block, bins=n_bins, density=False)
log_counts, log_edges = nmpy.histogram(
    nmpy.log(block + 1.0), bins=n_bins, density=False
)

print(
    f"Total:{block.size} = Background:{nmpy.count_nonzero(block <= 50)} + Objects:{nmpy.count_nonzero(block > 50)}"
)

pypl.matshow(block)
pypl.figure()
pypl.hist(block.flat, bins=edges, label="Plain")
pypl.hist(nmpy.log(block + 1.0).flat, bins=log_edges, label="Log")
pypl.legend()
pypl.show()


# --- Background Subtraction Tests
# images = (nmpy.ones(shape), 5.0 * nmpy.random.random(shape), shapes)
images = (shapes,)
segmentation = nmpy.dstack(images)

for image in images:
    for log_mode in (True, False):
        # result, background_small, background = WithBackgroundSubtracted(
        #     image, block_size, log_mode=log_mode, should_return_background=True
        # )
        result = WithBackgroundSubtracted(image, segmentation, mode="interpolated")

        pypl.matshow(image), pypl.colorbar()
        # pypl.matshow(background_small), pypl.colorbar()
        # pypl.matshow(background), pypl.colorbar()
        pypl.matshow(result), pypl.colorbar()

        pypl.show()
