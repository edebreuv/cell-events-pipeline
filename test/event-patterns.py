import catalog.event.death as death
import catalog.event.division as dvsn
import matplotlib.pyplot as pypl
import numpy as nmpy
import task.filtering as fltr

SIGNAL_LENGTH = 300

pattern_division = dvsn.FilterForEntropy() + 1
pattern_death = death.FilterForEntropy()

signal_division = nmpy.ones((SIGNAL_LENGTH,))
signal_division[0 : len(pattern_division)] = pattern_division
signal_division = nmpy.roll(signal_division, int(round(2.0 * (SIGNAL_LENGTH / 3.0))))

signal_death = nmpy.zeros((SIGNAL_LENGTH,))
signal_death[0 : len(pattern_death)] = pattern_death
rolling_extent = int(round(SIGNAL_LENGTH / 3.0))
signal_death = nmpy.roll(signal_death, rolling_extent)
signal_death[0:rolling_extent] = 1

response_division = fltr.PatternMatchingResponse(
    signal_division, dvsn.FilterForEntropy(), "Division"
)
response_death = fltr.PatternMatchingResponse(
    signal_death, death.FilterForEntropy(), "Death"
)

_, axes = pypl.subplots(ncols=2)

axes[0].plot(signal_division, label="signal w/ division")
axes[0].plot(signal_death, label="signal w/ death")
axes[0].plot(response_division, linestyle="--", label="division response")
axes[0].plot(response_death, linestyle="--", label="death response")
axes[0].legend()

axes[1].plot(pattern_division)
axes[1].plot(pattern_death)

pypl.show()
