"""
Notes about tensorflow:
pip install --user ml_dtypes==0.2.0
"""
import sys as sstm

import better_exceptions as xcpt
import cell_tracking_BC.catalog.processing.registration as rgst
import cell_tracking_BC.in_out.graphics.sequence as qvwr
import cell_tracking_BC.in_out.text.plot as pltx
import napari as npri
import numpy as nmpy
from cell_tracking_BC.catalog.feature.temporal.shift import AddShiftFeature
from cell_tracking_BC.catalog.processing.background import WithBackgroundSubtracted
from cell_tracking_BC.catalog.segmentation.tf_network import InputSizeOfTFNetwork
from cell_tracking_BC.in_out.file.event import SaveCellEventsToXLSX
from cell_tracking_BC.in_out.file.feature import SaveCompartmentsFeaturesToXLSX
from cell_tracking_BC.in_out.file.feature_book import SaveFeatureBookToJSON
from cell_tracking_BC.in_out.file.path import StorageFolderForMain
from cell_tracking_BC.in_out.file.track import SaveTrackDetailsToXLSX
from cell_tracking_BC.in_out.graphics.annotations import SaveSequenceAnnotations
from cell_tracking_BC.in_out.graphics.dbe.matplotlib.event import (
    PlotDivisionAndDeathEventResponses,
)
from cell_tracking_BC.in_out.graphics.event import ShowDivisionAndDeathEventResponses
from cell_tracking_BC.type.acquisition.sequence import frames_t, sequence_t
from cell_tracking_BC.type.analysis import analysis_t
from im_tools_36.input import ImageVolumeOrSequence
from im_tools_36.output import SaveSequenceAsTIFF
from logger_36 import LOGGER, AddFileHandler, AddRichConsoleHandler
from logger_36.catalog.logging.chronos import LogElapsedTime
from logger_36.catalog.logging.gpu import LogGPURelatedDetails
from logger_36.catalog.logging.memory import LogMemoryUsages
from logger_36.catalog.logging.system import LogSystemDetails
from logger_36.task.storage import SaveLOGasHTML

from config.channel import ChannelsToBeComputed
from config.feature import FeaturesToBeComputed
from pipeline.a_config import PipelineConfiguration
from pipeline.c_segmentation import CNNBasedCellSegmentation
from pipeline.d_tracking import CellTracks
from pipeline.f_analysis import (
    DeathResponsesAndCellMarking,
    DivisionResponsesAndCellMarking,
)
from pipeline.z_show import (
    ShowAllResults,
    ShowNCells,
    ShowSegmentationDetails,
    ShowTracking,
)

# from cell_tracking_BC.version import version


array_t = nmpy.ndarray


VERSION = "2024.1"
_LOG_FILE_NAME = "info-warning-error-log"


xcpt.hook()


# if VERSION != version:
#     print(
#         f'{VERSION}! Pipeline version incompatible with "cell_tracking_BC" version ({version})'
#     )
#     sstm.exit(1)

AddRichConsoleHandler(show_memory_usage=True, should_hold_messages=True)
LOGGER.ToggleLogInterceptions(True)
LOGGER.info(f"READING PIPELINE CONFIGURATION")
config_path, config_as_dict, config = PipelineConfiguration()
storage_folder = StorageFolderForMain(
    config.pipeline__result_folder,
    config.pipeline__structure_result_folder,
    config.sequence__name,
    config.sequence__first_frame,
    config.sequence__last_frame,
)
AddFileHandler(storage_folder / f"{_LOG_FILE_NAME}.txt", show_memory_usage=True)


LogSystemDetails()
LogGPURelatedDetails()
LOGGER.info(
    f"CONFIGURATION from: {config_path}\n"
    f"SAVING PIPELINE STAGES in folder: {storage_folder}"
)
formatted = "\n    ".join(str(config).splitlines())
LOGGER.info(f"Configuration\n    {formatted}")


analysis = analysis_t(config_path=config_path, config=config_as_dict)


LOGGER.info(f"READING SEQUENCE: {config.sequence__folder / config.sequence__name}")
frames = ImageVolumeOrSequence(
    config.sequence__folder / config.sequence__name,
    with_module=config.sequence__loading_module,
    should_print_module=True,
)
if not isinstance(frames, array_t):
    LOGGER.error("\n".join(frames))
    sstm.exit(1)
# import subprocess as prss
# process = prss.Popen(("fiji", str(config.sequence__folder / config.sequence__name)))
# import matplotlib.pyplot as pypl
# first_frame_of_first_channel = frames[0, 0, ...]
# pypl.matshow(first_frame_of_first_channel)
if config.segmentation__networks[0].suffix.lower() == ".h5":
    expected_shape = None
else:
    expected_shape = InputSizeOfTFNetwork(config.segmentation__networks[0])
sequence = sequence_t.NewFromFrames(
    frames,
    config.sequence__channels,
    config.sequence__default_plot_channel,
    config.sequence__folder / config.sequence__name,
    first_frame=config.sequence__first_frame,
    last_frame=config.sequence__last_frame,
    expected_shape=expected_shape,
)
analysis.sequence = sequence
LOGGER.info(f"Sequence\n{sequence}")
pltx.PlotImage(
    frames[0, 0, ...], title=f'First frame of channel "{config.sequence__channels[0]}"'
)
if config.sequence__show:
    as_array = nmpy.moveaxis(sequence.AsArray(), (0, 1, 2), (2, 1, 0))
    _ = npri.view_image(as_array)
    npri.run()
    # qvwr.ShowSequence(analysis)
if config.sequence__show_statistics:
    qvwr.ShowSequenceStatistics(analysis.sequence)


LOGGER.info(
    f"REGISTRATION on {config.registration__reference_channel}: {config.registration__register_channels}"
)
if config.registration__register_channels:
    channel_names = sequence.channels
    where_reference = channel_names.index("YFP")
    where_main_target = channel_names.index("CFP")
    translations = []
    for f_idx, channels in enumerate(sequence.FramesIterator()):
        reference = channels[where_reference]
        main_target = channels[where_main_target]
        translation = rgst.TranslationFromTo(reference, main_target)
        if any(_trl != 0.0 for _trl in translation):
            translations.append(f"{f_idx}:{tuple(-translation)}")
            for name, channel in zip(channel_names, channels):
                if (name != "YFP") and (name != "POL"):
                    translated = rgst.WithTranslationApplied(channel, translation)
                    sequence[name][f_idx] = translated
    if translations.__len__() > 0:
        translations = ", ".join(translations)
        LOGGER.info(f"Translations: {translations}")
if config.registration__show_registered_channels:
    SaveSequenceAsTIFF(
        sequence.AsArray(),
        storage_folder / "registered-channels.tiff",
        channels=sequence.channels,
    )
    # qvwr.ShowSequence(analysis)

# first_frame_of_first_channel = sequence["CFP"][0]
# pypl.matshow(first_frame_of_first_channel)
# pypl.show()

LOGGER.info(f"CELL SEGMENTATION on {config.segmentation__channels}")
segmentations = CNNBasedCellSegmentation(sequence, storage_folder, config)
#
analysis.segmentations = segmentations
ShowSegmentationDetails(segmentations)
if config.segmentation__show:
    as_array = segmentations.AsArray()
    if as_array.ndim == 4:
        channels = ("cells",) + tuple(
            f"compartment_{_idx}" for _idx in range(as_array.shape[2] - 1)
        )
    else:
        channels = None
    SaveSequenceAsTIFF(
        as_array, storage_folder / "segmentation.tiff", channels=channels
    )
    # gvwr.ShowSegmentation(analysis)


LOGGER.info("ADD COMPUTED CHANNEL(S)")
if config.channels__channels_for_background is not None:
    for channel in config.channels__channels_for_background:
        name = f"{channel}{config.channels__background_suffix}"

        computed = frames_t()
        for frame, segmentation in zip(sequence[channel], segmentations):
            subtracted = WithBackgroundSubtracted(
                frame, segmentation.cells_map, mode="median"
            )
            computed.append(subtracted)

        sequence[name] = computed

for name, Function in ChannelsToBeComputed(config):
    print(f"    Channel: {name}...")
    sequence.AddChannel(name, Function)
LOGGER.info(f"Sequence with Computed Channel(s)\n{sequence}")
if config.channels__show_computed_channels:
    SaveSequenceAsTIFF(
        sequence.AsArray(),
        storage_folder / "computed-channels.tiff",
        channels=sequence.channels,
    )
    # qvwr.ShowSequence(analysis)


LOGGER.info("CELL CREATION")
segmentations.BuildCellsFromMaps()
analysis.segmentations = segmentations
ShowNCells(segmentations)


LOGGER.info("CELL TRACKING")
tracks = CellTracks(segmentations, config)
if tracks.__len__() == 0:
    LOGGER.warning("No valid tracks found")
    sstm.exit(1)
analysis.tracks = tracks
tracks.PrintValidInvalidSummary(logger=LOGGER)
if config.tracking__show:
    ShowTracking(analysis)


LOGGER.info("CELL FEATURES")
for description in FeaturesToBeComputed(sequence, config):
    if description.channel is None:
        segmentations.AddCompartmentFeature(
            description.compartment, description.name, description.computation
        )
    else:
        if isinstance(description.channel, str):
            frames = sequence[description.channel]
        else:
            frames = tuple(
                tuple(sequence[_chl][_idx] for _chl in description.channel)
                for _idx in range(sequence.length)
            )
        segmentations.AddCompartmentFeature(
            description.compartment,
            description.name,
            description.computation,
            frames=frames,
        )
AddShiftFeature(tracks)


LOGGER.info("ANALYSIS - Event Detection")
division_responses = DivisionResponsesAndCellMarking(analysis, config)
death_responses = DeathResponsesAndCellMarking(analysis, config)

# Leave time point extractions here so that dividing and death markings could have happened in the desired order
if division_responses is None:
    division_time_points = None
else:
    division_time_points = tracks.DivisionTimePoints()
if death_responses is None:
    death_time_points = None
else:
    death_time_points = tracks.DeathTimePoints(sequence.length, with_living_leaves=True)

if config.analysis__show_division_and_death_responses:
    ShowDivisionAndDeathEventResponses(
        division_responses,
        death_responses,
        division_time_points,
        death_time_points,
        sequence.length,
        show_as_barplot=True,
        PlotAndShow=PlotDivisionAndDeathEventResponses,
    )


LOGGER.info("SAVING RESULTS")
SaveTrackDetailsToXLSX(storage_folder, analysis)
feature_book = SaveCompartmentsFeaturesToXLSX(storage_folder, analysis)
update = SaveCellEventsToXLSX(
    storage_folder,
    analysis,
    division_response="division_response",
    death_response="death_response",
)
feature_book.update(update)
SaveFeatureBookToJSON(feature_book, storage_folder)
LOGGER.warning(f"SAVING RESULTS: Sequence Annotations; /!\\ Potentially long task...")
SaveSequenceAnnotations(storage_folder, analysis)
for document in storage_folder.glob("*"):
    extension = document.suffixes[0][1:]
    if extension == "txt":
        continue
    type_folder = document.parent / extension
    type_folder.mkdir(exist_ok=True)
    document.rename(type_folder / document.name)
SaveLOGasHTML(storage_folder / f"{_LOG_FILE_NAME}.htm")


LOGGER.info("PIPELINE ENDED")
LogMemoryUsages(max_n_samples=20)
LogElapsedTime()
if config.final_display__show_all_results_at_end:
    LOGGER.info("SHOWING RESULTS")
    ShowAllResults(
        analysis,
        division_responses,
        death_responses,
        division_time_points,
        death_time_points,
    )
