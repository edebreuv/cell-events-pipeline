from typing import Callable, Dict, Sequence, Tuple

from catalog.channel.rescaled_pol import RescaledPOLChannel
from cell_tracking_BC.catalog.channel.ratio import ChannelRatioFunction
from numpy import ndarray as array_t
from pipeline.a_config import config_t

channel_computation_h = Callable[[Dict[str, array_t]], array_t]


def ChannelsToBeComputed(
    config: config_t, /
) -> Sequence[Tuple[str, channel_computation_h]]:
    """"""
    output = [(config.features__entropy_channel, RescaledPOLChannel)]

    if config.channels__ratio_channels is not None:
        for channels in config.channels__ratio_channels:
            name = f"{channels[0]}{config.channels__ratio_symbol}{channels[1]}"
            Function = ChannelRatioFunction(channels[0], channels[1])
            output.append((name, Function))

    return output
