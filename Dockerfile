#####
## Docker container for running the cell events detection pipeline
## Dynamic analysis framework to detect cell division and cell death events in 
## live-cell imaging experiments, using signal processing and machine learning
## This dynamic analysis framework is a computational pipeline which runs an 
## automatic analysis on a live-cell time-lapse microscopy, to detect cell 
## divisions and cell deaths. 
##
## Based off the tensorrt image from Nvidia. It will complain about not having
## GPUs, but these are not necessary to run the pipeline.
## The image has Python 3.10 by default and the following packages: 
## Package           Version
##----------------- --------
## appdirs           1.4.4
## graphsurgeon      0.4.6
## Mako              1.2.4
## MarkupSafe        2.1.3
## numpy             1.23.5
## Pillow            10.0.0
## pip               23.2.1
## platformdirs      3.10.0
## polygraphy        0.49.0
## protobuf          4.23.4
## pycuda            2022.2.2
## pytools           2023.1.1
## setuptools        59.6.0
## tensorrt          8.6.1
## typing_extensions 4.7.1
## uff               0.6.9
## wheel             0.37.1


FROM nvcr.io/nvidia/tensorrt:23.08-py3

LABEL maintainer "Neva C. Durand <nchernia@gmail.com>"

# Install tracking and segmentation; this will also install some of the 
# necessary modules for the next step
COPY dependencies.txt ./

RUN pip install --upgrade pip \
   && pip install -r ./dependencies.txt
WORKDIR /software
COPY . /software

ENTRYPOINT ["python3", "run_pipeline.py"]
