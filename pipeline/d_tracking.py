from cell_tracking_BC.catalog.tracking.gmb_tracker import gmb_tracker_t
from cell_tracking_BC.type.segmentation.sequence import segmentations_t
from cell_tracking_BC.type.track.multiple.structured import tracks_t
from cell_tracking_BC.type.track.single.structured import BasicTrackIssues
from pipeline.a_config import config_t


def CellTracks(segmentations: segmentations_t, config: config_t, /) -> tracks_t:
    """"""
    tracker = gmb_tracker_t(
        shape=segmentations[0].shape,
        track_cells=config.tracking__track_cells,
        min_jaccard=config.tracking__min_jaccard_for_tracking,
        division_min_jaccard=config.tracking__min_jaccard_for_cell_division,
    )
    output = tracker.Run(segmentations.length, segmentations.cells_iterator)
    _BasicTrackIssues = lambda _tck: BasicTrackIssues(
        _tck,
        root_time_point_interval=config.tracking__root_time_point_interval,
        min_duration=config.tracking__min_track_length,
    )
    output.FilterOut(_BasicTrackIssues)

    return output
