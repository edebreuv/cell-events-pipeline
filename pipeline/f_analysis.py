from typing import Dict, Optional, Sequence

import catalog.event.death as death
import catalog.event.division as dvsn
import task.filtering as fltr
from cell_tracking_BC.type.analysis import analysis_t
from pipeline.a_config import config_t


def DivisionResponsesAndCellMarking(
    analysis: analysis_t, config: config_t, /
) -> Optional[Dict[int, Optional[Sequence[float]]]]:
    """"""
    if config.analysis__division_threshold_for_entropy is None:
        division_responses = None
        analysis.tracks.MarkDividingCells(False)
    else:
        analysis.tracks.AddFeature(
            "division_response",
            config.features__entropy_feature,
            fltr.PatternMatchingResponse,
            dvsn.FilterForEntropy(),
            "Division",
        )
        division_responses = analysis.tracks.Feature("division_response")
        analysis.tracks.MarkDividingCells(
            False,
            division_responses=division_responses,
            lower_bound=config.analysis__division_threshold_for_entropy,
        )

    return division_responses


def DeathResponsesAndCellMarking(
    analysis: analysis_t, config: config_t, /
) -> Optional[Dict[int, Optional[Sequence[float]]]]:
    """"""
    if config.analysis__death_threshold_for_entropy is None:
        death_responses = None
    else:
        analysis.tracks.AddFeature(
            "death_response",
            config.features__entropy_feature,
            fltr.PatternMatchingResponse,
            death.FilterForEntropy(),
            "Death",
        )
        death_responses = analysis.tracks.Feature("death_response")
        analysis.tracks.MarkDeadCells(
            death_responses,
            config.analysis__death_threshold_for_entropy,
            config.analysis__trust_division_more_than_death,
            config.analysis__event_detection_method,
        )

    return death_responses
