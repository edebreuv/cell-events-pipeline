from argparse import ArgumentParser as argument_parser_t
from pathlib import Path as path_t
from typing import Any, Dict, Tuple

from conf_ini_g.light.config import config_t as base_config_t
from conf_ini_g.light.ini import NewConfigFromPath


class config_t(base_config_t):
    analysis__death_threshold_for_entropy: float | None
    analysis__division_threshold_for_entropy: float | None
    analysis__event_detection_method: str
    analysis__show_division_and_death_responses: bool
    analysis__trust_division_more_than_death: bool
    channels__background_suffix: str
    channels__channels_for_background: list[str]
    channels__ratio_channels: list[tuple[str, str]] | None
    channels__ratio_symbol: str
    channels__show_computed_channels: bool
    features__cell_map_margin: float
    features__entropy_cell_map_margin: float
    features__entropy_channel: str
    features__entropy_feature: str
    features__n_bins_for_entropy_computation: int
    final_display__show_all_results_at_end: bool
    pipeline__result_folder: path_t
    pipeline__structure_result_folder: bool
    registration__reference_channel: str
    registration__register_channels: bool
    registration__show_registered_channels: bool
    segmentation__channels: list[tuple[str, str]]
    segmentation__max_area_discrepancy_for_fused_cell_splitting: float
    segmentation__max_compartment_hole_area: list[int]
    segmentation__min_compartment_area: list[int]
    segmentation__min_jaccard_for_fused_cell_splitting: float
    segmentation__network_thresholds: list[float]
    segmentation__networks: list[path_t]
    segmentation__show: bool
    segmentation__show_prediction: bool
    segmentation__temporal_gap_filling: int
    sequence__channels: list[str]
    sequence__default_plot_channel: str
    sequence__first_frame: int
    sequence__folder: path_t
    sequence__last_frame: int | None
    sequence__loading_module: str | None
    sequence__name: str
    sequence__show: bool
    sequence__show_statistics: bool
    tracking__max_frames_under_min_area: int
    tracking__min_cell_area_along_track: int
    tracking__min_jaccard_for_cell_division: float
    tracking__min_jaccard_for_tracking: float
    tracking__min_track_length: int
    tracking__root_time_point_interval: tuple[int, int]
    tracking__show: bool
    tracking__track_cells: bool


def PipelineConfiguration() -> Tuple[path_t, Dict[str, Any], config_t]:
    """"""
    parser = argument_parser_t(
        description="Cell Division and Death Events Detection", allow_abbrev=False
    )

    parser.add_argument(
        "--config", required=True, type=str, help="Path to pipeline configuration file"
    )

    parser.add_argument(
        "--structure_result_folder",
        type=str,
        choices=("True", "False"),
        help="Whether the result folder should be structured using runtime-related sub-folders",
    )

    name_1 = "[sequence]folder"
    name_2 = "[sequence]name"
    parser.add_argument(
        "--sequence",
        type=str,
        help=f'Path to sequence (overrides "{name_1}" and "{name_2}" in configuration file)',
    )

    name = "trust_division_more_than_death"
    parser.add_argument(
        "--trust",
        type=str,
        choices=("division", "death"),
        help=f"Which event detection is trusted the most "
        f'(overrides "{name}" in configuration file)',
    )

    name = "result_folder"
    parser.add_argument(
        "--result_folder",
        type=str,
        help=f'Path to result folder (overrides "{name}" in configuration file',
    )

    name = "death_threshold_for_entropy"
    parser.add_argument(
        "--death",
        type=float,
        help=f"Death threshold for entropy signal "
        f'(overrides "{name}" in configuration file)',
    )

    name = "event_detection_method"
    parser.add_argument(
        "--detection_method",
        type=str,
        choices=("first above", "first max", "global max"),
        help=f"Death event detection method "
        f'(overrides "{name}" in configuration file)',
    )

    arguments = parser.parse_args()

    config_path = path_t(arguments.config).resolve(strict=True)
    as_dict = NewConfigFromPath(config_path)
    if arguments.structure_result_folder is not None:
        as_dict["Pipeline"]["structure_result_folder"] = (
            arguments.structure_result_folder == "True"
        )
    if arguments.sequence is not None:
        new_path = path_t(arguments.sequence)
        as_dict["Sequence"]["folder"] = new_path.parent
        as_dict["Sequence"]["name"] = new_path.name
    if arguments.trust is not None:
        as_dict["Analysis"]["trust_division_more_than_death"] = (
            arguments.trust == "division"
        )
    if arguments.death is not None:
        as_dict["Analysis"]["death_threshold_for_entropy"] = arguments.death
    if arguments.detection_method is not None:
        as_dict["Analysis"]["event_detection_method"] = arguments.detection_method

    if arguments.result_folder is not None:
        as_dict["Pipeline"]["result_folder"] = path_t(arguments.result_folder)

    if "loading_module" not in as_dict["Sequence"]:
        as_dict["Sequence"]["loading_module"] = None

    as_class = config_t.NewFromDictionary(as_dict, path=config_path)

    return config_path, as_dict, as_class
