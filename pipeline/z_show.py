from typing import Dict, Optional, Sequence

import cell_tracking_BC.in_out.graphics.tracking as tvwr
import cell_tracking_BC.in_out.text.plot as pltx
from cell_tracking_BC.in_out.graphics.dbe.matplotlib.d_any import figure_t as m_figure_t
from cell_tracking_BC.in_out.graphics.dbe.matplotlib.event import (
    PlotDivisionAndDeathEventResponses,
)
from cell_tracking_BC.in_out.graphics.event import ShowDivisionAndDeathEventResponses
from cell_tracking_BC.type.analysis import analysis_t
from cell_tracking_BC.type.segmentation.sequence import segmentations_t

# from logger_36 import LOGGER
# from psutil import Process as process_t

# _PROCESS = process_t()
# _GIGA_SCALING = 1.0 / (1024**3)
# _max_used_memory = 0.0


def ShowSegmentationDetails(segmentations: segmentations_t, /) -> None:
    """"""
    print("+++ Cell area histogram (after segm. correction + border clearing)")
    areas = segmentations.cell_areas
    pltx.PlotHistogram(areas, center_decimals=0)

    compartments = "TODO"
    print(f"+++ Segmentation compartment(s): {compartments}")


def ShowNCells(segmentations: segmentations_t, /) -> None:
    """"""
    n_cells_at_0 = segmentations.NCells(in_frame=0)
    n_cells = segmentations.NCells()
    print(f"+++ Number of Cells: Frame 0={n_cells_at_0}, Total={n_cells}")


def ShowTracking(sequence: analysis_t, /) -> None:
    """"""
    # Keep figure assignments independent (no "_ = ..."'s for example) to maintain figures references
    _1 = tvwr.ShowInvalidTracking3D(sequence, with_cell_labels=False, prepare_only=True)
    _2 = tvwr.ShowInvalidTracking2D(sequence, with_cell_labels=False, prepare_only=True)
    _3 = tvwr.ShowTracking3D(sequence, with_cell_labels=False, prepare_only=True)
    _4 = tvwr.ShowTracking2D(sequence, with_cell_labels=False, prepare_only=True)

    m_figure_t.ShowAll()


def ShowAllResults(
    sequence: analysis_t,
    division_response: Dict[int, Sequence[float]],
    death_response: Dict[int, Sequence[float]],
    division_idc: Dict[int, Optional[Sequence[int]]],
    death_idc: Dict[int, Optional[int]],
    /,
) -> None:
    """"""
    # Keep figure assignments independent (no "_ = ..."'s for example) to maintain figures references
    _2 = tvwr.ShowInvalidTracking3D(sequence, with_cell_labels=False, prepare_only=True)
    _3 = tvwr.ShowInvalidTracking2D(sequence, with_cell_labels=False, prepare_only=True)
    _4 = tvwr.ShowTracking3D(sequence, with_cell_labels=False, prepare_only=True)
    _5 = tvwr.ShowTracking2D(sequence, with_cell_labels=False, prepare_only=True)
    ShowDivisionAndDeathEventResponses(
        division_response,
        death_response,
        division_idc,
        death_idc,
        sequence.sequence.length,
        show_as_barplot=True,
        prepare_only=True,
        PlotAndShow=PlotDivisionAndDeathEventResponses,
    )

    m_figure_t.ShowAll()


# def PrintMemoryUsage(*, only_max: bool = False) -> None:
#     """"""
#     global _max_used_memory
#
#     used_memory = round(_GIGA_SCALING * _PROCESS.memory_info().rss, 1)
#     _max_used_memory = max(used_memory, _max_used_memory)
#     if only_max:
#         LOGGER.info(f">>> MAX. MEMORY USAGE: {_max_used_memory}Gb >>>")
#     else:
#         LOGGER.info(
#             f">>> Memory usage: {used_memory}Gb | Max. {_max_used_memory}Gb >>>"
#         )
