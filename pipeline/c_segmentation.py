from pathlib import Path as path_t

import cell_tracking_BC.task.segmentation.sequence as sgmt
from cell_tracking_BC.catalog.processing.geometry import WithSmallObjectsAndHolesRemoved
from cell_tracking_BC.catalog.segmentation.tf_network import SegmentationsWithTFNetwork
from cell_tracking_BC.type.acquisition.sequence import sequence_t
from cell_tracking_BC.type.segmentation.sequence import segmentations_t
from config.unet import UNetInstance
from im_tools_36.output import SaveSequenceAsTIFF
from pipeline.a_config import config_t


def CNNBasedCellSegmentation(
    sequence: sequence_t, storage_folder: path_t, config: config_t, /
) -> segmentations_t:
    """"""
    segmentations_as_dct = {_key: None for _key in ("cells", "cytoplasms", "nuclei")}

    for (
        (name, channel),
        path,
        threshold,
        min_compartment_area,
        max_compartment_hole_area,
    ) in zip(
        config.segmentation__channels,
        config.segmentation__networks,
        config.segmentation__network_thresholds,
        config.segmentation__min_compartment_area,
        config.segmentation__max_compartment_hole_area,
    ):
        # --- Initial Segmentation
        PostProcessed = lambda _frm: WithSmallObjectsAndHolesRemoved(
            _frm, min_compartment_area, max_compartment_hole_area
        )
        segmentations, predictions = SegmentationsWithTFNetwork(
            sequence[channel],
            path,
            LoadFromWeights=UNetInstance,
            threshold=threshold,
            PostProcessed=PostProcessed,
        )
        if config.segmentation__show_prediction:
            SaveSequenceAsTIFF(predictions, storage_folder / f"prediction-{name}.tiff")
        if config.segmentation__show:
            SaveSequenceAsTIFF(
                segmentations, storage_folder / f"segmentation-initial-{name}.tiff"
            )

        # --- Segmentation Correction
        sgmt.FillTemporalGaps(segmentations, config.segmentation__temporal_gap_filling)
        if config.segmentation__show:
            SaveSequenceAsTIFF(
                segmentations,
                storage_folder / f"segmentation-w-filled-gaps-{name}.tiff",
            )

        # --- Segmentation Correction
        if name == "cells":
            _ = sgmt.CorrectBasedOnTemporalCoherence(
                segmentations,
                min_jaccard=config.segmentation__min_jaccard_for_fused_cell_splitting,
                max_area_discrepancy=config.segmentation__max_area_discrepancy_for_fused_cell_splitting,
                min_cell_area=min_compartment_area,
            )
            if config.segmentation__show:
                SaveSequenceAsTIFF(
                    segmentations,
                    storage_folder / f"segmentation-w-temporal-coherence-{name}.tiff",
                )

        segmentations_as_dct[name] = segmentations

    assert segmentations_as_dct["cells"] is not None

    cells_maps, _, nuclei_maps = sgmt.AllCompartmentsFromSome(
        cells_maps=segmentations_as_dct["cells"],
        cytoplasms_maps=segmentations_as_dct["cytoplasms"],
        nuclei_maps=segmentations_as_dct["nuclei"],
    )

    return segmentations_t.NewFromCellsMaps(cells_maps, nuclei_maps=nuclei_maps)
