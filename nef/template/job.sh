HOMEDIR=__PLACEHOLDER-HOMEDIR__
PROJECT=__PLACEHOLDER-PROJECT__
BASE_OUTPUT_FOLDER=$HOMEDIR/output-$PROJECT

# [ $# != 3 ] && { echo 'Syntax: '"$0"' parameters_path sequence_path trust_division_or_death'; exit 1; }
parameters=$1
sequence=$2
trust=$3

sequence_name=`basename "$sequence" | sed -n -e s/\\\...$// -e s/\\\....$// -e s/\\\.....$// -e p`
date=$(date +'%F_%H.%M.%S_%N')
output_folder="$BASE_OUTPUT_FOLDER/$sequence_name/$date"
mkdir -p "$output_folder"

module load conda/2020.11-python3.8
python3.8 $HOMEDIR/$PROJECT/run_pipeline.py \
    --parameters "$parameters" \
    --sequence "$sequence" \
    --trust "$trust" \
    --workspace "$output_folder/workspace.pkl" \
    --features "$output_folder/cell_features.xlsx" \
    --events "$output_folder/cell_events.xlsx" \
    --annotated "$output_folder/annotated_sequence.tif" \
        > "$output_folder/stdout.txt" \
        2> "$output_folder/stderr.txt"
