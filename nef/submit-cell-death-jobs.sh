# CHECK TERMINATION STATUS with: oarstat -fj <JOBID>
OAR_CONFIG="$HOME"/cell-death-oar-config.sh # Must be absolute path
NEEDED_MEMORY_IN_GB=6
oarsub -S "$OAR_CONFIG" -p 'mem > '$NEEDED_MEMORY_IN_GB'000' -t besteffort
