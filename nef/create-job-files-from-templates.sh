PROJECT=cell-death

templates="job.sh jobs-parameters.txt oar-config.sh"
template_folder=template
for template in $templates
do
    name="$PROJECT"-"$template"
    sed -n \
        -e 's:__PLACEHOLDER-HOMEDIR__:'"$HOME"':g' \
        -e 's:__PLACEHOLDER-PROJECT__:'"$PROJECT"':g' \
        -e p \
        "$template_folder/$template" \
    > "$name"
    chmod 700 "$name"
done
